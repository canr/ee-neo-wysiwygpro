<?php

$capCode = '
			var editor = WPro.##name##;					/* Get editor */
			var selected = editor.getSelectedHTML();	/* Get selected text */
			var strtLoc = selected.indexOf("<img");		/* Find beginning of img string */
			
			/* img tag found */
			if(strtLoc != -1)
			{
				var preText = selected.substr(0,strtLoc);	/* html before img */
				var imgStr = selected.substr(strtLoc);		/* Image html */
				var endLoc = imgStr.indexOf(">");			/* End location of image string */
				var postText = imgStr.substr(endLoc+1);		/* html after img */
				imgStr = imgStr.substr(0, endLoc+1);
				
				/* Spawn popup to get caption settings */
				/* First get caption settings */
				var cappop = $("<p><div></div></p>");
				$("<p>Caption Text:</p>").appendTo(cappop);
				var capinput = $("<textarea style=\"width:90%;\" rows=\"3\" class=\"capinput\"></textarea>");
				capinput.appendTo(cappop);
				
				/* Alignment settings */
				$("<span>Alignment: </span>").appendTo(cappop);
				var capalign =	$("<select>\
									<option>left</option>\
									<option>center</option>\
									<option>right</option>\
								</select>");
				capalign.appendTo(cappop);
				
				/* Text wrap settings */
				$("<br /><br /><span>Align Text With Image: </span>").appendTo(cappop);
				var capwrap = $("<input type=\"checkbox\" name=\"wrap\" checked />");
				capwrap.appendTo(cappop);
				
				/* Use to make sure only one instance of the caption box is launched at once */
				if(typeof(editor["cap"]) === "undefined")
				{
					editor["cap"] = 0;
				}

				/* Begin definition of popup box */
				$(cappop).dialog({
					modal: true,
					autoOpen: true,
					resizable: false,
					title: "Caption Settings",
					open: function()
					{
						/* This code included to avoid repetetive calls to the jquery dialog */
						editor["cap"] += 1;
						if(editor["cap"] > 1)
						{
							$(this).dialog("close");
						}
					
						var prevcap = postText.replace(/<span class=\"caption-text\".*?>((.|\n)*?)<\/span>(.|\n)*/, "$1");
						if(prevcap != postText)
						{
							prevcap = prevcap.replace(/<.*?>/g, "");	/* Remove HTML tags */
							$(capinput).val(prevcap);
						}
					},
					close: function()
					{
						editor["cap"] -= 1;
					},
					/* Add buttons to confirm or cancel caption settings */
					buttons:{
								"Okay": function()
								{
									var text = $(capinput).val();
									var align = $(capalign).val();
									var wrap = !($(capwrap).is(":checked"));
									/*alert(postText);*/
									/* Remove any previous caption formatting */
									var len1 = preText.length;
									var len2 = preText.replace(/<p class=\"img-caption\".*?>/,"").length;
									 
									postText = postText.replace(/<span class=\"caption-text\".*?>.*?<\/span>/,"");
									
									/* If a p tag can be removed in pretext, remove pre-existing tags */
									if(len1 > len2)
									{
										preText = preText.replace(/<p class=\"img-caption\".*?>/, "");
										postText = postText.replace(/<\/p>/, "");
									}
									
									if(wrap)
									{
										var pretextalign = "float:" + align + ";";
										var posttextalign = "text-align:center;";
									}
									else
									{
										var pretextalign = "text-align:" + align + ";";
										var posttextalign = "";
									}
									
									/* Format output text */
									preText += "<p class=\"img-caption\" style=\"" + pretextalign + "padding:8px 8px 4px 0;\">";
									postText = "<span class=\"caption-text\" style=\"display:block;color:#666;font-style:italic;margin:0;padding:0 0 5px;" + posttextalign + "\">" + text + "</span>" + postText;
									
									var endstr = " " + preText + imgStr + postText + " ";
									editor.insertAtSelection(endstr);
									
									$(this).dialog("close");
								},
								"Cancel": function()
								{
									$(this).dialog("close");
								}
							}
				});
			}';
			
?>