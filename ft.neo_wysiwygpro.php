<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class neo_wysiwygpro_ft extends EE_Fieldtype {

	var $info = array(
		'name'	=> 'NeoWysiwygPro',
		'version'	=> '1.0'
	);
	
	var $has_array_data = false;

	// constructor used to inherit the base class
	function neo_wysiwygpro_ft()
	{
		parent::EE_Fieldtype();
	}
	
	// --------------------------------------------------------------------

	// validate the field input
	function validate($data)
	{
		return true;
	}
	
	// --------------------------------------------------------------------
	
	// render the publish field
	function display_field($data)
	{
		// include the wysiwygPro library
		include_once('E:\inetpub\Expengdev\wysiwygPro\wysiwygPro.class.php');

		// Instantiate the editor 
		${$this->field_name} = new wysiwygPro();
		
		// Name the editor for the form
		${$this->field_name}->name = $this->field_name;
		
		// Initialize the editor content
		${$this->field_name}->value = htmlspecialchars_decode($data);
		
		//If the site_id is not equal to 1 then the site is running under the multiple site manager plugin
		//Also, there are a lot of other fun variables to grabbed from $this->settings
        $doc_root = ""; // used to add the /sites/(yoursite) folders to directories if we are not on the main site
        $doc_string = ""; // used to add the channel folder if we are on the main site
        $site_name = "http://expengdev.anr.msu.edu/";
        if($this->settings["site_id"] != 1)
		{
            // get the site shortname and URL from the database
            $this->EE->db->select('site_name, site_description');
            $this->EE->db->where('site_id', $this->settings["site_id"]);
            $this->EE->db->limit(1);
            $query = $this->EE->db->get('exp_sites');
            $site_name = $query->row()->site_name;
            $doc_root = "/sites/".$site_name;  
          
            // set the wysiwyg editor's basedir to the current domain name
            $site_name = $query->row()->site_description;
            ${$this->field_name}->baseURL = $site_name;
		}
		else
		{
            // use a per-channel folder if this is not a MSM managed site
			if (isset($_GET["channel_id"]))
				$doc_string = "/".$_GET["channel_id"];
		}
		
        if($doc_root != "" OR $doc_string != "")
		{
			// create the content directories if they do not yet exist
			$content_directories = array( $_SERVER['DOCUMENT_ROOT'] . $doc_root . '/uploads',
								$_SERVER['DOCUMENT_ROOT'] . $doc_root . '/uploads/files',
								$_SERVER['DOCUMENT_ROOT'] . $doc_root . '/uploads/images',
								$_SERVER['DOCUMENT_ROOT'] . $doc_root . '/uploads/media',
								$_SERVER['DOCUMENT_ROOT'] . $doc_root . '/uploads/files' . $doc_string,
								$_SERVER['DOCUMENT_ROOT'] . $doc_root . '/uploads/images' . $doc_string,
								$_SERVER['DOCUMENT_ROOT'] . $doc_root . '/uploads/media' . $doc_string
								);
				
			foreach ($content_directories as $directory)
			{
				if (!is_dir($directory)) {mkdir($directory);}
			}
			
			// Configure the file browser and add any other configuration options you require:
			
			// Full file path of your documents folder & URL:
			${$this->field_name}->documentDir = $_SERVER['DOCUMENT_ROOT'] . $doc_root . '/uploads/files' . $doc_string;
			${$this->field_name}->documentURL = $site_name . 'uploads/files' . $doc_string;
			${$this->field_name}->maxDocSize = '10 MB';
			${$this->field_name}->allowedDocExtensions = '.html, .htm, .pdf, .rtf, .txt, .doc, .docx, .xls, .xlsx, .ppt, .pptx, .pps, .ppsx, .xlsm, .sdlb, .sdrt';
		
			// Full file path of your images folder & URL:
			${$this->field_name}->imageDir = $_SERVER['DOCUMENT_ROOT'] . $doc_root . '/uploads/images' . $doc_string;
			${$this->field_name}->imageURL = $site_name . 'uploads/images' . $doc_string;
			${$this->field_name}->maxImageSize = '5 MB';
			${$this->field_name}->allowedImageExtensions = '.jpg, .jpeg, .gif, .png, .ps, .eps, .psd, .ai, .svg, .tif, .tiff';

			// Full file path of your media folder & URL:
			${$this->field_name}->mediaDir = $_SERVER['DOCUMENT_ROOT'] . $doc_root . '/uploads/media' . $doc_string;
			${$this->field_name}->mediaURL = $site_name . 'uploads/media' . $doc_string;
			${$this->field_name}->maxMediaSize = '10 MB';
			${$this->field_name}->allowedMediaExtensions = '.swf, .flv, .wmv, .wma, .wax, .wvx, .asf, .rm, .mov, .mp3, .mp4, .h264, .avi, .mpg';
		
			// set file browser editing permissions:
			${$this->field_name}->editImages = true;
			${$this->field_name}->renameFiles = true;
			${$this->field_name}->renameFolders = true;
			${$this->field_name}->deleteFiles = true;
			${$this->field_name}->deleteFolders = true;
			${$this->field_name}->copyFiles = true;
			${$this->field_name}->copyFolders = true;
			${$this->field_name}->moveFiles = true;
			${$this->field_name}->moveFolders = true;
			${$this->field_name}->upload = true;
			${$this->field_name}->overwrite = true;
			${$this->field_name}->createFolders = true;
		}
			
		// customize buttons/features
		${$this->field_name}->htmlVersion = 'XHTML 1.0 Strict';
		${$this->field_name}->disableFeatures(array('previewtab')); 
		${$this->field_name}->clearToolbarLayout();
		${$this->field_name}->addToolbar( "custom", array("undo","redo","separator1",
											"cut","copy","pastecleanup","separator2",
											"styles","font","size","fontcolor","separator3",
											"bold","underline","italic","strikethrough","separator4",
											"left","center","right","separator5",
											"numbering","bullets","defaultruler","tablemenu","separator6",
											"link","bookmark","image","specialchar","separator7",
											"find","spelling","fullwindow"
										));
		//${$this->field_name}->addStyle('div class="linksCol"', 'MSUE Links Column');
		
		//Add caption button
		include(PATH_THIRD.'neo_wysiwygpro/scripts/min_caption.php');
		${$this->field_name}->addbutton('Caption', 'after:image', $capCode, '/wysiwygPro/images/caption.gif');
		
		// force the editor to put pasted code through its cleanup filter
		${$this->field_name}->addJSEditorEvent("load", "function(evt) { WPro.events.addEvent(WPro.##name##.editDocument.body, 'paste', function (evt) { WPro.##name##.plugins['wproCore_codeCleanup'].open('##name##'); WPro.preventDefault(evt); }); }");
		
		// Render the editor at 300px high
		$editor = ${$this->field_name}->fetch('100%', 300);
		
		return $editor;
	}
	
	// --------------------------------------------------------------------
	
	// replace the custom field tag on the frontend
	function replace_tag($data, $params = '', $tagdata = '')
	{
		return $this->EE->typography->parse_type(
			$this->EE->functions->encode_ee_tags($data),
			array(
				'text_format'	=> $this->row['field_ft_'.$this->field_id],
				'html_format'	=> $this->row['channel_html_formatting'],
				'auto_links'		=> $this->row['channel_auto_link_urls'],
				'allow_img_url' 	=> $this->row['channel_allow_img_urls']
			)
		);
	}
	
	// --------------------------------------------------------------------
	
	// custom field settings displayed on Edit Field screen
	function display_settings($data)
	{
		//$this->field_formatting_row($data, "wysiwygpro");
	}
	
	// --------------------------------------------------------------------	
	
	//Edit text field content before saving
	function save($data)
	{	
		//Get filepaths to necessary functions
		$acronymizer_path = PATH_THIRD.'/acronymizer/pi.acronymizer.php';
		$external_links_path = PATH_THIRD.'/external_links/pi.external_links.php';
		
		//Include classes
		include_once($acronymizer_path);
		include_once($external_links_path);
		
		//Declare class instances
		$acronymizer = new acronymizer;
		$external_links = new external_links;
	
		//Edit content
		$data = $acronymizer->acronymize($data);
		$data = $external_links = $external_links->tag_external_links($data);
		
		return $data;
	}
}

?>